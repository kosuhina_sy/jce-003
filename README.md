TASK-MANAGER
========================
Учебный проект в рамках курса JAVA SPING.

Требования к sowtware
--------------------------
  Openjdk version "11"
  
  Apache Maven-3.6.3

Описание стека технологий
-----------------------------
  Java + Apache Maven

Разработчик
---------------------------------
  Светлана Косухина
 
  e-mail <lana__svet@list.ru>  

Команда для сборки приложения
------------------------------------
  mvn package

Команда для запуска приложения
-------------------------------------
 java -jar task-manager-1.0.0.jar

Поддерживаемые терминальные команды
-------------------------------------

  help -отображение списка терминальных команд
  
  version - отображение информации о версии приложения
  
  about - отображение данных о разработчике
    







